//
//  AppDelegate.h
//  TestPanModelObjc
//
//  Created by Pavel Nefedov on 19/04/2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

