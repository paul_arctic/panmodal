//
//  PresentationNavigationController.swift
//  TestPanModelObjc
//
//  Created by Александр Нефёдов on 20.04.2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

import UIKit
import PanModal

extension PresentationNavigationController: PanModalPresentable {
    
    // MARK: - override
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    open override func popViewController(animated: Bool) -> UIViewController? {
        let viewController = super.popViewController(animated: animated)
        return viewController
    }
    
    open override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
    }
    
    // MARK: - Pan Modal Presentable
    
    public var panScrollable: UIScrollView? {
        return (topViewController as? PanModalPresentable)?.panScrollable
    }
    
    public var longFormHeight: PanModalHeight {
        return .maxHeight
    }
    
    public var shortFormHeight: PanModalHeight {
        return longFormHeight
    }
}
