//
//  NavPresentetionViewController.h
//  TestPanModelObjc
//
//  Created by Александр Нефёдов on 20.04.2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NavPresentetionViewController : UIViewController

@property (nonatomic, strong, readonly) UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
