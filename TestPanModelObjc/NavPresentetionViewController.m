
//
//  NavPresentetionViewController.m
//  TestPanModelObjc
//
//  Created by Александр Нефёдов on 20.04.2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

#import "NavPresentetionViewController.h"
#import "PresentationViewController.h"

@interface NavPresentetionViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong, nonnull) UITableView *tableView;

@end

@implementation NavPresentetionViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        UITableView *table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        table.delegate = self;
        table.dataSource = self;
        [table registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        [self.view addSubview:table];
        
        self.tableView = table;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - TableViewDatasource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"asdasdasd"];
    cell.textLabel.text = @"Переход";
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NavPresentetionViewController *vc = [[NavPresentetionViewController alloc] initWithNibName:nil bundle:nil];
    if (self.navigationController != nil) {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
