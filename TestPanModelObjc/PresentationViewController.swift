//
//  PresentationViewController.swift
//  TestPanModelObjc
//
//  Created by Pavel Nefedov on 19/04/2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

import UIKit
import PanModal

extension PresentationViewController: PanModalPresentable {
	public var panScrollable: UIScrollView? {
		return tableView
	}
}
