//
//  NavPresentetionViewController.swift
//  TestPanModelObjc
//
//  Created by Александр Нефёдов on 20.04.2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

import UIKit
import PanModal

extension NavPresentetionViewController: PanModalPresentable {
    public var panScrollable: UIScrollView? {
        return tableView
    }
}
