//
//  SceneDelegate.h
//  TestPanModelObjc
//
//  Created by Pavel Nefedov on 19/04/2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

