//
//  PresentationViewController.m
//  TestPanModelObjc
//
//  Created by Pavel Nefedov on 19/04/2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

#import "PresentationViewController.h"

@interface PresentationViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end

@implementation PresentationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - TableViewDatasource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"asdasdasd"];
	cell.textLabel.text = @"Старт";
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 50.;
}


@end
