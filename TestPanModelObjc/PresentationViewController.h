//
//  PresentationViewController.h
//  TestPanModelObjc
//
//  Created by Pavel Nefedov on 19/04/2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PresentationViewController : UIViewController

@property (nonatomic, strong, readonly) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
