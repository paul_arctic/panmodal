//
//  ViewController.m
//  TestPanModelObjc
//
//  Created by Pavel Nefedov on 19/04/2020.
//  Copyright © 2020 Pavel Nefedov. All rights reserved.
//

#import "ViewController.h"
#import "PresentationViewController.h"
#import "PresentationNavigationController.h"
#import "NavPresentetionViewController.h"
@import PanModal;
//#import <PanModal/PanModal-Swift.h>


@interface ViewController ()

@property (nonatomic, strong, nullable) PanModalPresentationDelegate *transitioningDelegate;

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)onShowTap:(id)sender {
	UIStoryboard * stb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	PresentationViewController *vc = [stb instantiateViewControllerWithIdentifier:@"PresentationViewController"];
	[self presentPanModal:vc];
//	PanModalPresentationDelegate *delegate = [PanModalPresentationDelegate new];
//	vc.modalPresentationStyle = UIModalPresentationCustom;
//	vc.transitioningDelegate = delegate;
//
//	[self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)onShowTapNavigation:(id)sender {
    NavPresentetionViewController *vc = [[NavPresentetionViewController alloc] initWithNibName:nil bundle:nil];
    
    PresentationNavigationController *navVC = [[PresentationNavigationController alloc] initWithRootViewController:vc] ;
    [self presentPanModal:navVC];
}

@end
